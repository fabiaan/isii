FROM node
COPY . /var/www
WORKDIR /var/www
RUN npm install
ENV PORT 7070
EXPOSE 7070
ENTRYPOINT [ "npm", "start" ]