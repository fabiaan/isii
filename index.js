const express = require('express')
const cors = require('cors')
const path = require('path')
//Middleware para Vue.js router modo history
const history = require('connect-history-api-fallback')

const app = express()

var corsOptions = {
  origin: '*', // Reemplazar con dominio
  optionsSuccessStatus: 200
}
app.use(cors(corsOptions));

app.use(history())

const URL = path.join(__dirname,'/public')
app.use(express.static(URL))
console.log(URL)

// Servidor
const PORT = process.env.PORT || 4002
app.listen(PORT, ()=>{
  console.log(`Servidor funcionando en el puerto ${PORT}`)
})